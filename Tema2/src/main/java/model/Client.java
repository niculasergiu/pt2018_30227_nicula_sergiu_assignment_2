package model;

public class Client {

    private int nrClient;
    private int nrDeProduce;
    public Client(int nrClient, int nrDeProduce)
    {
        this.nrClient=nrClient;
        this.nrDeProduce=nrDeProduce;
    }
    public int getNrClient(){
        return this.nrClient;
    }
    public int getNrDeProduce(){
        return this.nrDeProduce;
    }
     public String toString(){
        return "Clientul "+nrClient +" are " + nrDeProduce+" produse";
    }


}
