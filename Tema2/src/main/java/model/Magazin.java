package model;
import javax.swing.*;
import java.util.*;
import java.text.DecimalFormat;

public class Magazin implements Runnable {
    private static DecimalFormat formatMedie = new DecimalFormat("#.##");
    private int timpTotalSimulare;
    private int nrDeCase;
    private int timpServireMin;
    private int timpServireMax;
    private int timpSosireMin;
    private int timpSosireMax;
    private int timpSimulare;
    private List<Casa> listaCase;
    private boolean finish = false;
    private JTextArea istoric;
    private JLabel timp;
    private GenerareClienti gc;
    private JTextArea simulare;
    private String simulare1;
    private int viteza;
    private JLabel ajunge;
    private JLabel pleaca;

    public Magazin(int timpTotalSimulare, int nrDeCase, int timpServireMin, int timpServireMax, int timpSosireMin, int timpSosireMax, JTextArea istoric, JLabel timp,JTextArea simulare,int viteza,JLabel ajunge,JLabel pleaca) {
        this.timpTotalSimulare = timpTotalSimulare;
        this.nrDeCase = nrDeCase;
        listaCase = new ArrayList<>();
        this.timpServireMin = timpServireMin;
        this.timpServireMax = timpServireMax;
        this.timpSosireMin = timpSosireMin;
        this.timpSosireMax = timpSosireMax;
        this.istoric = istoric;
        this.timp=timp;
        this.simulare=simulare;
        this.viteza=viteza;
        this.ajunge=ajunge;
        this.pleaca=pleaca;
    }

    public void run() {
        for (int i = 1; i <= nrDeCase; i++) {
            Casa c = new Casa(i,viteza);
            listaCase.add(c);
            Thread t = new Thread(c);
            t.start();
        }
        for (Casa c : listaCase) {
            c.setTimpTotalSimulare(timpTotalSimulare);
            c.setTimpSosireMax(timpSosireMax);
            c.setTimpServireMax(timpServireMax);
        }

        gc = new GenerareClienti(timpSosireMin, timpSosireMax, timpServireMin, timpServireMax, listaCase,timpTotalSimulare,viteza);
        istoric.append("Start\n");
        Thread t1 = new Thread(gc);
        t1.start();
        while (true) {
            timpSimulare++;
            try {
                Thread.sleep(viteza);
            } catch (Exception e) {
            }
            synchronized (this) {
                String p = "";
                String a = "";
                for (Casa c : listaCase) {
                    c.setTimpSimulare(timpSimulare);
                    if(c.getMaxInCoada()<c.timpTotalInCoada()){
                        c.setMaxInCoada(c.timpTotalInCoada());
                        c.setOraV(timpSimulare);
                    }
                    c.timpDeschis();
                    p = p + c.getClientP();
                    a = a + c.getClientA();
                }
                ajunge.setText(a);
                pleaca.setText(p);


                simulare1 = "\n";
                gc.setTimpSimulare(timpSimulare);
                timp.setText(timpSimulare + "");
                for (Casa c : listaCase) {
                    istoric.append(c.toString());
                    simulare1 = simulare1 + "                         La casa " + c.getNrCasa() + "(" + c.timpTotalInCoada() + ")" + "\n\n" + c.afisareCoada() + "\n\n";
                }
                simulare.setText(simulare1);

                if (timpSimulare == timpTotalSimulare) {
                    finish = true;
                    istoric.append("Finish\n");
                    for (Casa c : listaCase) {
                        float medie = 0;
                        medie = c.getTimpDeschis() / c.getNrClienti();
                        istoric.append("Media de asteptat la casa " + c.getNrCasa() + " este de " + formatMedie.format(medie) + "  secunde" + "\n");
                        istoric.append("Secunda de varf la casa "+c.getNrCasa()+" este "+c.getOraV()+", timp de asteptare "+ c.getMaxInCoada()+ " secunde "+"\n");
                        istoric.append("Casa "+c.getNrCasa()+ " a fost inchisa "+(timpTotalSimulare-c.getTimpDeschis())+" secunde"+"\n");
                    }
                    try {
                        wait();
                    } catch (Exception e) {
                    }
                }
            }
        }
    }
}


