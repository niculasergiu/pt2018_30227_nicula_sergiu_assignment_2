package model;

import java.util.*;
import java.lang.*;

public class Casa implements Runnable
{
    private Queue<Client> c;
    private int nrCasa ;
    private int timpTotalSimulare;
    private int timpSosireMax;
    private int timpServireMax;
    private float nrC;
    private int timpSimulare;
    private String ajunge="";
    private String pleaca="";
    private String scaneaza="";
    private int timpDeschisa;
    private boolean ok=true;
    private int viteza;
    private String clientP="";
    private String clientA="";
    private int oraV=0;
    private int maxInCoada=0;

    public Casa(int nrCasa,int viteza)
    {
        c= new ArrayDeque<>();
        this.nrCasa=nrCasa;
        this.viteza=viteza;
    }
    public void setTimpSimulare(int s){
        this.timpSimulare=s;
    }
    public void setTimpTotalSimulare(int timpTotalSimulare) {
        this.timpTotalSimulare = timpTotalSimulare;
    }

    public void setTimpSosireMax(int timpSosireMax) {
        this.timpSosireMax = timpSosireMax;
    }

    public void setTimpServireMax(int timpServireMax) {
        this.timpServireMax = timpServireMax;
    }

    public int getOraV() {
        return oraV;
    }

    public int getMaxInCoada() {
        return maxInCoada;
    }

    public void setMaxInCoada(int maxInCoada) {
        this.maxInCoada = maxInCoada;
    }

    public void setOraV(int oraV) {
        this.oraV = oraV;
    }

    public synchronized void insert(Client a)
    {
        int d=timpSimulare+1;
        if(timpTotalSimulare>=timpTotalInCoada()+timpServireMax+timpSosireMax+d && ok==true){
            clientA=a.getNrClient()+"";
            ajunge=ajunge+"Timp total de asteptare  la casa " + nrCasa + " inainte de sosire: " + timpTotalInCoada()+"\n"+"La casa " + nrCasa + " a sosit clientul " + a.getNrClient() + " la secunda " + d+"\n";
            c.add(a);
            notify();
        }
        else
        {
            ok=false;
        }
    }
    public String toString(){
        String a=this.ajunge+this.scaneaza+this.pleaca;
        ajunge="";
        scaneaza="";
        pleaca="";
        return a;
    }
    public String getClientP() {
        String a=this.clientP;
        clientP="";
        return a;
    }
    public String getClientA(){
        String a=this.clientA;
        clientA="";
        return a;
    }

    public String afisareCoada()
    {
        String s="";
        for (Client cNou:c ){
            s=s+"              | "+cNou.toString()+" | "+"\n";
        }
        if (c.size()==0){
            s="                       CASA INCHISA";
        }
        return s;
    }

    public int timpTotalInCoada()
    {
        int timp=0;
        for (Client cNou:c ) {
            timp = timp + cNou.getNrDeProduce();
        }
        return timp;
    }
    public void timpTotalCasa(){
        nrC++;
    }
    public void timpDeschis(){
        if (c.size()>0){
            timpDeschisa++;
        }
    }
    public int getTimpDeschis(){
       return timpDeschisa;
    }
    public float getNrClienti(){
        return nrC;
    }
    public int getNrCasa(){
        return nrCasa;
    }
    public void run()
    {
        while (true)
        {
                if (c.size() > 0) {
                    if (timpTotalSimulare >= timpSimulare + c.element().getNrDeProduce()) {
                        scaneaza = scaneaza+ "Scaneaza clientul " + c.element().getNrClient() + " la casa " + nrCasa + "\n";
                        try {
                            Thread.sleep(c.element().getNrDeProduce() * viteza);
                        } catch (Exception e) {
                        }
                        int a=timpSimulare+1;
                        clientP=c.element().getNrClient()+"";
                        pleaca = pleaca + "La casa " + nrCasa + " a plecat clientul " + c.element().getNrClient() + " la secunda " + a + "\n";
                        timpTotalCasa();
                        c.remove();
                    }
                }
            synchronized(this)
            {
                if (c.size()==0)
                    try{
                        wait();
                    }catch (Exception e){}
            }
        }
    }
}
