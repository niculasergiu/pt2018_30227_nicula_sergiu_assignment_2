package model;
import java.util.*;

public class GenerareClienti implements Runnable{

    private int timpSosireMin;
    private int timpSosireMax;
    private int timpServireMin;
    private int timpServireMax;
    private int casaA;
    private int id = 1;
    private List<Casa> listaCase;
    private int timpTotalSimulare;
    private int timpSimulare;
    private boolean ok;
    private boolean ok1=true;
    private int viteza;

    public GenerareClienti (int timpSosireMin, int timpSosireMax, int timpServireMin, int timpServireMax,List<Casa> listaCase, int timpTotalSimulare,int viteza) {
        this.timpSosireMin = timpSosireMin;
        this.timpSosireMax = timpSosireMax;
        this.timpServireMin = timpServireMin;
        this.timpServireMax = timpServireMax;
        this.listaCase = listaCase;
        this.timpTotalSimulare = timpTotalSimulare;
        this.viteza=viteza;
    }

    public int nrCl(){
        id=id+1;
        return id;
    }

    public void run() {
        while (true) {

            if (ok1 == true) {
                try {
                    Thread.sleep(viteza / 50);
                    ok1 = false;

                } catch (Exception e) {
                }
            }
            try {
                Thread.sleep(timpSosire() * viteza);
            } catch (Exception e) {
            }
            ok = true;
            Client cl = new Client(id, timpServire());
            nrCl();
            casaA = Integer.MAX_VALUE;
            for (Casa c : listaCase) {
                if (c.timpTotalInCoada() < casaA) {
                    casaA = c.timpTotalInCoada();
                }
            }
            for (Casa c : listaCase) {
                if (ok == true) {
                    if (c.timpTotalInCoada() == casaA) {
                        c.insert(cl);
                        ok = false;
                    }
                }
            }
            synchronized (this) {
                if (timpSimulare == timpTotalSimulare - timpServireMax - timpSosireMax) {
                    try {
                        wait();
                    } catch (Exception e) {
                    }
                }
            }
        }
    }

    public void setTimpSimulare(int s){
        this.timpSimulare=s;
    }
    public int timpSosire(){
        Random rand = new Random();
        int timp =rand.nextInt(timpSosireMax-timpSosireMin+1)+timpSosireMin;
        return timp;
    }

    public int timpServire(){
        Random rand = new Random();
        int timp =rand.nextInt(timpServireMax-timpServireMin+1)+timpServireMin;
        return timp;
    }

}
