package gui;
import model.*;
import javax.swing.*;
import java.awt.*;

public class MyFrame extends JFrame {

    private JTextArea istoric = new JTextArea("");
    private JScrollPane scrollPane = new JScrollPane(istoric);
    private JTextArea simulare = new JTextArea("");
    private JScrollPane scrollPane1 = new JScrollPane(simulare);

    private JButton butonStart = new JButton("START");

    private JTextField viteza = new JTextField("1000");
    private JTextField timpTS = new JTextField("Timp total de simulare");
    private JTextField nrDC = new JTextField("Numar de case");
    private JTextField timpSoMin = new JTextField("Timp de sosire  min");
    private JTextField timpSoMax = new JTextField("Timp de sosire  max");
    private JTextField timpSeMin = new JTextField("Timp de servire min");
    private JTextField timpSeMax = new JTextField("Timp de servire max");

    private JLabel timp =  new JLabel("");
    private JLabel init = new JLabel("Initializare");
    private JLabel istS = new JLabel("Istoric Simulare");
    private JLabel sim = new JLabel("Simulare");

    private JLabel ajunge = new JLabel("");
    private JLabel pleaca = new JLabel("");

    private JLabel intrare= new JLabel("тег");
    private JLabel iesire= new JLabel("тее");

    public MyFrame(String title){
        setTitle(title);
        setSize(1000,600);
        setResizable(false);
        JPanel panel = new JPanel();
        panel.setLayout(null);
        panel.setEnabled(true);
        panel.setVisible(true);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        setContentPane(panel);
        Font boldT = new Font("", Font.BOLD, 18);
        Font boldE = new Font("", Font.BOLD, 12);
        Font boldS = new Font("", Font.BOLD, 14);
        Font boldc = new Font("", Font.BOLD, 100);
        Font bolda = new Font("", Font.BOLD, 30);


        butonStart.setBounds(175,200,200,40);
        butonStart.setFont(boldT);
        butonStart.setHorizontalAlignment(JTextField.CENTER);

        timpTS.setBounds(100,60,150,20);
        nrDC.setBounds(300,60,150,20);
        timpTS.setFont(boldE);
        nrDC.setFont(boldE);
        timpTS.setHorizontalAlignment(JTextField.CENTER);
        nrDC.setHorizontalAlignment(JTextField.CENTER);

        timpSoMin.setBounds(100,100,150,20);
        timpSoMax.setBounds(300,100,150,20);
        timpSoMin.setFont(boldE);
        timpSoMax.setFont(boldE);
        timpSoMin.setHorizontalAlignment(JTextField.CENTER);
        timpSoMax.setHorizontalAlignment(JTextField.CENTER);

        timpSeMin.setBounds(100,140,150,20);
        timpSeMax.setBounds(300,140,150,20);
        timpSeMin.setFont(boldE);
        timpSeMax.setFont(boldE);
        timpSeMin.setHorizontalAlignment(JTextField.CENTER);
        timpSeMax.setHorizontalAlignment(JTextField.CENTER);


        init.setBounds(225,10,200,50);
        init.setFont(boldT);

        timp.setBounds(850,10,50,50);
        timp.setFont(boldT);

        istS.setBounds(680,10,140,50);
        istS.setFont(boldT);

        sim.setBounds(235,250,300,50);
        sim.setFont(boldT);

        scrollPane.setBounds(550,60,400,490);
        scrollPane1.setBounds(125,300,300,250);

        intrare.setFont(boldc);
        iesire.setFont(boldc);
        intrare.setHorizontalAlignment(JTextField.CENTER);
        iesire.setHorizontalAlignment(JTextField.CENTER);
        intrare.setBounds(30,300,100,250);
        iesire.setBounds(425,300,100,250);

        ajunge.setFont(bolda);
        pleaca.setFont(bolda);
        ajunge.setHorizontalAlignment(JLabel.CENTER);
        pleaca.setHorizontalAlignment(JLabel.CENTER);
        ajunge.setBounds(30,355,100,250);
        pleaca.setBounds(425,250,100,250);


        panel.setBackground(Color.CYAN);
        timp.setBackground(Color.BLUE);
        simulare.setBackground(Color.YELLOW);
        simulare.setFont(boldS);
        istoric.setBackground(Color.YELLOW);
        istoric.setFont(boldS);
        butonStart.setBackground(Color.GREEN);

        viteza.setBounds(570,25,100,20);
        viteza.setFont(boldE);
        viteza.setHorizontalAlignment(JTextField.CENTER);
        viteza.setBackground(Color.YELLOW);
        timpTS.setBackground(Color.YELLOW);
        nrDC.setBackground(Color.YELLOW);
        timpSeMin.setBackground(Color.YELLOW);
        timpSeMax.setBackground(Color.YELLOW);
        timpSoMin.setBackground(Color.YELLOW);
        timpSoMax.setBackground(Color.YELLOW);

        panel.add(ajunge);
        panel.add(pleaca);
        panel.add(viteza);
        panel.add(intrare);
        panel.add(iesire);
        panel.add(scrollPane);
        panel.add(scrollPane1);
        panel.add(timp);
        panel.add(istS);
        panel.add(init);
        panel.add(sim);
        panel.add(butonStart);

        panel.add(timpTS);
        panel.add(nrDC);

        panel.add(timpSeMin);
        panel.add(timpSeMax);

        panel.add(timpSoMin);
        panel.add(timpSoMax);
        butonStart.addActionListener(new ButonStart(timpTS,nrDC,timpSeMin,timpSeMax,timpSoMin,timpSoMax,istoric,timp,simulare,viteza,ajunge,pleaca));
    }


    public static void main(String[] args) {
        new MyFrame ("Lidl CLUJ NAPOCA");
    }
}
