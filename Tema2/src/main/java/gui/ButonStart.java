package gui;

import model.*;
import java.awt.event.*;
import javax.swing.*;

public class ButonStart implements ActionListener{
    private JTextField timpTotalSimulare;
    private JTextField nrDeCase;
    private JTextField timpServireMin;
    private JTextField timpServireMax;
    private JTextField timpSosireMin;
    private JTextField timpSosireMax;
    private int timpTotalSimulareInt;
    private int nrDeCaseInt;
    private int timpServireMinInt;
    private int timpServireMaxInt;
    private int timpSosireMinInt;
    private int timpSosireMaxInt;
    private JTextArea istoric;
    private JLabel timp;
    private JTextArea simulare;
    private JTextField viteza;
    private int vitezaInt;
    private JLabel ajunge;
    private JLabel pleaca;


    public ButonStart(JTextField timpTotalSimulare, JTextField nrDeCase, JTextField timpServireMin, JTextField timpServireMax, JTextField timpSosireMin, JTextField timpSosireMax,JTextArea istoric,JLabel timp,JTextArea simulare, JTextField viteza,JLabel ajunge,JLabel pleaca) {
        this.timpTotalSimulare = timpTotalSimulare;
        this.nrDeCase = nrDeCase;
        this.timpServireMin = timpServireMin;
        this.timpServireMax = timpServireMax;
        this.timpSosireMin = timpSosireMin;
        this.timpSosireMax = timpSosireMax;
        this.istoric = istoric;
        this.timp=timp;
        this.simulare=simulare;
        this.viteza=viteza;
        this.ajunge=ajunge;
        this.pleaca=pleaca;
    }
    public void actionPerformed (ActionEvent e){

        timpTotalSimulareInt=Integer.parseInt(timpTotalSimulare.getText());
        nrDeCaseInt=Integer.parseInt(nrDeCase.getText());
        timpServireMinInt=Integer.parseInt(timpServireMin.getText());
        timpServireMaxInt=Integer.parseInt(timpServireMax.getText());
        timpSosireMinInt=Integer.parseInt(timpSosireMin.getText());
        timpSosireMaxInt=Integer.parseInt(timpSosireMax.getText());
        vitezaInt=Integer.parseInt(viteza.getText());
        Magazin m = new Magazin(timpTotalSimulareInt,nrDeCaseInt,timpServireMinInt,timpServireMaxInt,timpSosireMinInt,timpSosireMaxInt,istoric,timp,simulare,vitezaInt,ajunge,pleaca);
        Thread t = new Thread(m);
        t.start();
    }
}


